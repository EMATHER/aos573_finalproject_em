# AOS573_FinalProject_EM

AOS 573 Final Project
By Emily Mather

Note: The attached yml file is a clone of the AOS573-Tutorials environment so that is the environment that should be used to run this notebook.

The primary aim of this project was for me to familiarize myself with geostationary satellite data and how that can be used to assess ecosystem function as this will likely be relevant to my research. While the project does not necessarily seek to answer any specific research questions, it explores the relationship between wildfires and vegetation health. I chose to use three different datasets, all from GOES-17, for my analysis. 

Part 1: Fire
The first dataset I chose is from August 2020, in the middle of wildfire season in the Western US. This dataset is from the level 2 Fire Dectection product, so it is processed data in which likely fires have been identified. Since the fires only occupied a small portion of the land surface, it can be challenging to spacially visualize their distribution. I read the dataset into satpy and then converted it to a geodataframe so that I could use the explore function in geopandas where you can zoom in and out and scroll around. 

This section also contains a group of plots looking at the intensity of the fires. This was just to get a senes of the range of fires captured by the GOES fire detection product since the intensity of the fire plays a significant role in determining the magnitude of its impace on the landscape.

Part 2: Plants
In this section I selected two more datasets, one from 3 months prior to the scene from part one, and another from 9 months after. The idea was to compare the two datasets from one year to the next, before and after burning, to see the impact of the burning on the vegetation. These two datasets are level 1 products so they contain the raw radiance as well as processed reflectance or brighness temperature for all 16 GOES ABI channels. I calculated the normalized difference vegetation index (NDVI) for both datasets using challels 2 and 3. 

This first plot in this section is of NDVI from the before fire scene. The purpose of this plot is to check and see if the data makes sense. In the southern part of the US, the higher NDVI valuse generally correspond to areas with more vegetation. However, some areas is the north part of the scene do not seem correct. The next figure shows the true_color composite of the scene which shows that there was significant cloudn cover over the land in the norhter part of the scene which is likely a cause for error. If I were to proceed with this analysis, I would maks the parts of the scene where clouds are present and only look at datapoints without cloud interference. The thir plot in this section shows a comparison of NDVI between the before and after fire scenes over a small area where fires were detected in the fire scene. 